export const BACKEND_URL = 'https://jsonplaceholder.typicode.com';

export const urls = {
  users: `${BACKEND_URL}/users/`,
  posts: `${BACKEND_URL}/posts/`,
  comments: (postId: number): string =>
    `${BACKEND_URL}/posts/${postId}/comments`,
};

export type ApiMethod = 'GET' | 'DELETE' | 'POST';
