import {
  Card,
  Theme,
  Modal,
  Button,
  TextField,
  makeStyles,
  CardHeader,
  createStyles,
} from '@material-ui/core';

import * as yup from 'yup';
import React, { FC, memo } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useDispatch, useSelector } from 'react-redux';
import Autocomplete from '@material-ui/lab/Autocomplete';

import { Post } from './types';
import { User } from '../users/types';
import { addNewPost } from './postsSlice';
import { selectAllUsers } from '../users/usersSlice';
import { Patterns } from '../../utils/constants/Patterns';

type Props = {
  isOpen: boolean;
  handleClose: () => void;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paper: {
      width: '70%',
      outline: 'none',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      backgroundColor: theme.palette.background.default,
      [theme.breakpoints.up('md')]: {
        width: '50%',
      },
      maxWidth: 700,
    },
    formControl: {
      marginBottom: 20,
      '& .MuiFormLabel-root.Mui-focused': {
        color: theme.palette.getContrastText(theme.palette.background.default),
      },
    },
    title: {
      textAlign: 'center',
    },
    btnSubmit: {
      maxWidth: 150,
    },
  }),
);

type FormFields = {
  user: User;
  title: string;
  body: string;
};

enum FieldNames {
  User = 'user',
  Title = 'title',
  Body = 'body',
}

const AddNewPost: FC<Props> = ({ isOpen, handleClose }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const validationSchema = yup.object().shape({
    [FieldNames.User]: yup.object().required('Name is required'),
    [FieldNames.Title]: yup
      .string()
      .required('Title is required')
      .min(8, 'Title must be at least 8 characters')
      .max(100, 'Title cannot be more than 100 characters')
      .matches(Patterns.Title, 'Please enter valid title'),
    [FieldNames.Body]: yup
      .string()
      .required('Post is required')
      .min(8, 'Post must be at least 8 characters')
      .max(400, 'Post cannot be more than 100 characters'),
  });

  const allUsers = useSelector(selectAllUsers);

  const { errors, control, handleSubmit } = useForm<FormFields>({
    mode: 'onChange',
    reValidateMode: 'onChange',
    resolver: validationSchema && yupResolver(validationSchema),
    defaultValues: Object.values(FieldNames).reduce(
      (p, n) => ({ ...p, [n]: n !== FieldNames.User ? '' : allUsers[0] }),
      {},
    ),
  });

  const onSubmit = handleSubmit(({ title, body, user }) => {
    const post: Post = {
      userId: user.id,
      id: new Date().getUTCMilliseconds(),
      title,
      body,
    };
    dispatch(addNewPost(post));
  });

  return (
    <Modal open={isOpen} onClose={handleClose} className={classes.modal}>
      <Card className={classes.paper}>
        <CardHeader title="Add new post" style={{ textAlign: 'center' }} />

        <form onSubmit={onSubmit} className="add-post-form">
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              margin: '20px 0px 20px 0px',
            }}>
            <Controller
              name={FieldNames.User}
              render={({ onChange }) => (
                <Autocomplete
                  options={allUsers}
                  onChange={(_, data) => onChange(data)}
                  getOptionLabel={(option) => option?.name}
                  noOptionsText="User does not exist"
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      autoFocus
                      margin="normal"
                      label="User name"
                      className={classes.formControl}
                      helperText={errors.user ? 'Name is required' : ''}
                      error={Boolean(errors.user)}
                    />
                  )}
                  renderOption={(option) => (
                    <>
                      <img
                        src={`https://reqres.in/img/faces/${option.id}-image.jpg`}
                        className="small-user-image"
                      />
                      {option.name}
                    </>
                  )}
                />
              )}
              control={control}
            />
            <Controller
              name={FieldNames.Title}
              as={
                <TextField
                  label="Title"
                  className={classes.formControl}
                  helperText={errors.title ? errors.title.message : ''}
                  error={Boolean(errors.title)}
                />
              }
              control={control}
            />
            <Controller
              name={FieldNames.Body}
              as={
                <TextField
                  id="body"
                  label="Your post"
                  multiline
                  rows={4}
                  variant="outlined"
                  className={classes.formControl}
                  helperText={errors.body ? errors.body.message : ''}
                  error={Boolean(errors.body)}
                />
              }
              control={control}
            />
          </div>

          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <Button
              variant="contained"
              color="primary"
              type="submit"
              className={classes.btnSubmit}>
              Submit
            </Button>
          </div>
        </form>
      </Card>
    </Modal>
  );
};

export default memo(AddNewPost);
