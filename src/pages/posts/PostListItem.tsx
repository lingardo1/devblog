import clsx from 'clsx';
import {
  Grid,
  Card,
  Theme,
  makeStyles,
  IconButton,
  CardHeader,
  Typography,
  CardContent,
  createStyles,
  CardActionArea,
} from '@material-ui/core';
import React, { FC, memo } from 'react';
import { NavLink } from 'react-router-dom';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

import { Post } from './types';
import { User } from '../users/types';
import Comments from '../../components/Comments/Comments';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      display: 'flex',
      backgroundColor: theme.palette.background.paper,
    },
    cardDetails: {
      flex: 1,
    },
    title: {
      textTransform: 'capitalize',
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    actionArea: {
      display: 'flex',
      padding: '10px 20px',
    },
    username: {
      textDecoration: 'none',
      color: theme.palette.getContrastText(theme.palette.background.default),
    },
  }),
);

type Props = {
  post: Post;
  user?: User;
  expanded: boolean;
  handleExpandClick: () => void;
  handleAlertOpen: (post: Post) => void;
};

const PostListItem: FC<Props> = ({
  post,
  user,
  expanded,
  handleAlertOpen,
  handleExpandClick,
}) => {
  const classes = useStyles();

  return (
    <Grid item xs={12} md={12}>
      <Card className={classes.card}>
        <div className={classes.cardDetails}>
          <CardHeader
            action={
              <IconButton onClick={() => handleAlertOpen(post)}>
                <DeleteForeverIcon />
              </IconButton>
            }
            title={post.title}
          />
          <CardContent>
            <Typography variant="subtitle1" color="textSecondary">
              {post.body}
            </Typography>
            <Typography
              variant="subtitle1"
              component={NavLink}
              className={classes.username}
              to={`/user-info/${user?.id}`}>
              {user?.username}
            </Typography>
          </CardContent>

          <CardActionArea
            onClick={handleExpandClick}
            className={classes.actionArea}>
            <Typography variant="subtitle1">Show comments</Typography>

            <ExpandMoreIcon
              className={clsx(classes.expand, {
                [classes.expandOpen]: expanded,
              })}
            />
          </CardActionArea>

          {expanded ? <Comments postId={post.id} /> : null}
        </div>
      </Card>
    </Grid>
  );
};

export default memo(PostListItem);
