import {
  createSlice,
  createSelector,
  createAsyncThunk,
  createEntityAdapter,
} from '@reduxjs/toolkit';

import { Post } from './types';
import { urls } from '../../config/api.config';
import { RootState } from '../../store/rootReducer';
import { selectAllUsers } from '../users/usersSlice';
import { send } from '../../utils/functions/api.function';

const postsAdapter = createEntityAdapter<Post>({
  sortComparer: (a, b) => a.title.localeCompare(b.title),
});

const initialState = postsAdapter.getInitialState({
  status: 'idle',
  error: null,
});

export const fetchPosts = createAsyncThunk(
  'posts/fetchPostsList',
  (): Promise<Post[]> => send(urls.posts, 'GET'),
);

export const addNewPost = createAsyncThunk(
  'posts/addNewPost',
  async (post: Post) => {
    await send(urls.posts, 'POST');
    return post;
  },
);

export const removePost = createAsyncThunk(
  'posts/removePost',
  async (post: Post) => {
    await send(urls.posts + post.id, 'DELETE');
    return post;
  },
);

const postsSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {
    postUpdated(state, action) {
      const { id, title, body } = action.payload;
      const existingPost = state.entities[id];
      if (existingPost) {
        existingPost.title = title;
        existingPost.body = body;
      }
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchPosts.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchPosts.fulfilled, (state, action) => {
        state.status = 'succeeded';
        postsAdapter.upsertMany(state, action.payload);
      })
      .addCase(fetchPosts.rejected, (state) => {
        state.status = 'failed';
      })
      .addCase(addNewPost.fulfilled, postsAdapter.addOne)
      .addCase(removePost.fulfilled, (state, action) => {
        const postId = state.ids.find(
          (el) => state.entities[el]?.id === action.payload.id,
        );
        if (postId !== undefined) {
          postsAdapter.removeOne(state, postId);
        }
      });
  },
});

export const { postUpdated } = postsSlice.actions;

export default postsSlice.reducer;

export const {
  selectAll: selectAllPosts,
  selectById: selectPostById,
  selectIds: selectPostIds,
} = postsAdapter.getSelectors<RootState>((state) => state.posts);

export const selectPostsByUser = createSelector(
  [
    selectAllPosts,
    selectAllUsers,
    (_state: RootState, searchedUser: string) => searchedUser,
  ],
  (posts, users, userName) => {
    const usersIds = users
      .filter((user) => user.username.toLowerCase().includes(userName))
      .map(({ id }) => id);
    return posts.filter((post) => usersIds.includes(post.userId));
  },
);
