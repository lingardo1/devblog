import {
  Grid,
  Input,
  Button,
  makeStyles,
  InputAdornment,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { useDispatch, useSelector } from 'react-redux';
import React, { FC, memo, useEffect, useMemo, useState } from 'react';
import SearchOutlined from '@material-ui/icons/SearchOutlined';

import { Post } from './types';
import AddNewPost from './AddNewPost';
import PostListItem from './PostListItem';
import { RootState } from '../../store/rootReducer';
import { selectAllUsers } from '../users/usersSlice';
import IconSortUp from '../../assets/sort-alpha-up.svg';
import IconSortDown from '../../assets/sort-alpha-down.svg';
import AlertDialog from '../../components/Alert/AlertDialog';
import { fetchPosts, removePost, selectPostsByUser } from './postsSlice';
import './styles.scss';
import { SortTypes } from '../../utils/types/sort.types';
import { getSortableData } from '../../utils/helpers/sortableData';

const useStyles = makeStyles({
  sortBtn: {
    marginRight: 10,
  },
});

const Posts: FC = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [searchedName, setSearchedName] = useState('');
  const [openNewPost, setOpenNewPost] = useState(false);
  const [showAlert, setShowAlert] = useState<Post | null>(null);

  const [openedIndex, setOpenedIndex] = useState<number>();
  const [sortOrder, setSortOrder] = useState(SortTypes.Asc);

  const allUsers = useSelector(selectAllUsers);

  const { status } = useSelector((state: RootState) => state.posts);

  const postsForUser = useSelector((state: RootState) =>
    selectPostsByUser(state, searchedName),
  );

  const sortedItems = useMemo(
    () => getSortableData(postsForUser, 'title', sortOrder),
    [postsForUser, sortOrder],
  );

  useEffect(() => {
    if (status === 'idle') {
      dispatch(fetchPosts());
    }
  }, [status, dispatch]);

  const onDelete = () => {
    if (showAlert) {
      dispatch(removePost(showAlert));
      setShowAlert(null);
    }
  };

  return (
    <>
      <div className="tool-panel">
        <div className="action-wrapper">
          <Button
            className={classes.sortBtn}
            onClick={() =>
              setSortOrder((prev) =>
                prev === SortTypes.Asc ? SortTypes.Desc : SortTypes.Asc,
              )
            }>
            {sortOrder === SortTypes.Asc ? <IconSortUp /> : <IconSortDown />}
          </Button>

          <Input
            onChange={(e) =>
              setSearchedName(e.target.value.trim().toLowerCase())
            }
            id="input-with-icon-adornment"
            startAdornment={
              <InputAdornment position="start">
                <SearchOutlined />
              </InputAdornment>
            }
          />
        </div>
        <div>
          <Button variant="outlined" onClick={() => setOpenNewPost(true)}>
            <AddIcon />
            Add New Post
          </Button>
        </div>
      </div>
      <div className="user-container">
        <Grid container spacing={2}>
          {sortedItems.map((post) => (
            <PostListItem
              post={post}
              key={post.id}
              handleAlertOpen={setShowAlert}
              handleExpandClick={() =>
                setOpenedIndex((prev) => (prev === post.id ? -1 : post.id))
              }
              expanded={openedIndex === post.id}
              user={allUsers.find(({ id }) => id === post.userId)}
            />
          ))}
        </Grid>
      </div>

      <AddNewPost
        handleClose={() => setOpenNewPost(false)}
        isOpen={openNewPost}
      />
      <AlertDialog
        handleConfirm={onDelete}
        open={Boolean(showAlert)}
        handleClose={() => setShowAlert(null)}
      />
    </>
  );
};

export default memo(Posts);
