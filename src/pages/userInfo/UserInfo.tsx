import * as yup from 'yup';
import { Alert } from '@material-ui/lab';
import { useParams } from 'react-router-dom';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useDispatch, useSelector } from 'react-redux';
import React, { FC, memo, useEffect, useState } from 'react';
import { Button, TextField, makeStyles } from '@material-ui/core';

import { RootState } from '../../store/rootReducer';
import { selectUserById, userUpdated } from '../users/usersSlice';
import './styles.scss';
import { Patterns } from '../../utils/constants/Patterns';

export enum FieldNames {
  Name = 'name',
  Username = 'username',
  Email = 'email',
  Phone = 'phone',
  Street = 'street',
  Suite = 'suite',
  City = 'city',
  Zipcode = 'zipcode',
}

const useStyles = makeStyles({
  formControl: {
    marginBottom: 20,
  },
  btnSubmit: {
    maxWidth: 150,
  },
});

const UserInfo: FC = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { id } = useParams<{ id: string }>();

  const [open, setOpen] = useState(false);

  const validationSchema = yup.object().shape({
    [FieldNames.Name]: yup
      .string()
      .required('Name is required')
      .min(8, 'Name must be at least 8 characters')
      .max(40, 'Name cannot be more than 40 characters')
      .matches(Patterns.FullName, 'Please enter valid title'),
    [FieldNames.Username]: yup
      .string()
      .required('Username is required')
      .min(4, 'Username must be at least 8 characters')
      .max(20, 'Username cannot be more than 40 characters')
      .matches(Patterns.UserName, 'Please enter valid title'),
    [FieldNames.Email]: yup
      .string()
      .required('Email is required')
      .email('Please enter valid email'),

    [FieldNames.Phone]: yup.string().required(),
    [FieldNames.Street]: yup.string().required(),
    [FieldNames.Suite]: yup.string().required(),
    [FieldNames.City]: yup.string().required(),
    [FieldNames.Zipcode]: yup.string().required(),
  });

  const { userId, formData } = useSelector((state: RootState) =>
    selectUserById(state, Number(id)),
  );

  const { reset, errors, control, handleSubmit } = useForm<
    { [key in FieldNames]: string }
  >({
    mode: 'onChange',
    reValidateMode: 'onChange',
    resolver: yupResolver(validationSchema),
    defaultValues: formData,
  });

  useEffect(() => {
    reset(formData);
  }, [reset, formData]);

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  const onSubmit = handleSubmit((data) => {
    if (userId) {
      dispatch(userUpdated({ ...data, userId }));
      setOpen(true);
      setTimeout(handleClose, 6000);
    }
  });

  return (
    <div>
      {open && (
        <Alert onClose={handleClose} severity="success">
          User successfully updated!
        </Alert>
      )}
      {formData ? (
        <form onSubmit={onSubmit} className="add-post-form">
          <div
            style={{
              margin: '20px 0px 20px 0px',
              display: 'flex',
              flexDirection: 'column',
            }}>
            {Object.values(FieldNames).map((name) => {
              return (
                <Controller
                  key={name}
                  name={name}
                  as={
                    <TextField
                      label={name}
                      className={classes.formControl}
                      helperText={errors[name] ? errors[name]?.message : ''}
                      error={Boolean(errors[name])}
                    />
                  }
                  control={control}
                />
              );
            })}
          </div>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <Button
              variant="outlined"
              type="submit"
              className={classes.btnSubmit}>
              Submit
            </Button>
          </div>
        </form>
      ) : (
        'User does not exist'
      )}
    </div>
  );
};

export default memo(UserInfo);
