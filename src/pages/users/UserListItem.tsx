import {
  Grid,
  Card,
  Theme,
  makeStyles,
  Typography,
  CardContent,
  createStyles,
  CardActionArea,
} from '@material-ui/core';
import React, { FC, memo } from 'react';
import { NavLink } from 'react-router-dom';

import { User } from '../users/types';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      display: 'flex',
      backgroundColor: theme.palette.background.paper,
    },
    cardDetails: {
      flex: 1,
    },
  }),
);

type Props = {
  user: User;
};

const UserListItem: FC<Props> = ({ user }) => {
  const classes = useStyles();

  return (
    <Grid item xs={12} md={6}>
      <CardActionArea component={NavLink} to={`/user-info/${user.id}`}>
        <Card className={classes.card}>
          <img
            src={`https://reqres.in/img/faces/${user.id}-image.jpg`}
            className="userImage"
          />
          <div className={classes.cardDetails}>
            <CardContent>
              <Typography component="h2" variant="h5">
                {user.name}
              </Typography>
              <Typography variant="subtitle1" color="textSecondary">
                {user.email}
              </Typography>
            </CardContent>
          </div>
        </Card>
      </CardActionArea>
    </Grid>
  );
};

export default memo(UserListItem);
