import {
  EntityId,
  createSlice,
  PayloadAction,
  createSelector,
  createAsyncThunk,
  createEntityAdapter,
} from '@reduxjs/toolkit';

import { User } from './types';
import { urls } from '../../config/api.config';
import { FieldNames } from '../userInfo/UserInfo';
import { RootState } from '../../store/rootReducer';
import { send } from '../../utils/functions/api.function';

const usersAdapter = createEntityAdapter<User>();

const initialState = usersAdapter.getInitialState({
  status: 'idle',
  error: null,
});

export const fetchUsers = createAsyncThunk(
  'posts/fetchUsers',
  (): Promise<User[]> => send(urls.users, 'GET'),
);

const usersSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    userUpdated(
      state,
      {
        payload,
      }: PayloadAction<{ [key in FieldNames]: string } & { userId: EntityId }>,
    ) {
      const existingPost = state.entities[payload.userId];
      if (existingPost) {
        existingPost.name = payload.name;
        existingPost.username = payload.username;
        existingPost.email = payload.email;
        existingPost.phone = payload.phone;
        existingPost.address.street = payload.street;
        existingPost.address.suite = payload.suite;
        existingPost.address.city = payload.city;
        existingPost.address.zipcode = payload.zipcode;
      }
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchUsers.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchUsers.fulfilled, (state, action) => {
        state.status = 'succeeded';
        // Add any fetched posts to the array
        usersAdapter.upsertMany(state, action.payload);
      })
      .addCase(fetchUsers.rejected, (state) => {
        state.status = 'failed';
        // state.error = action.payload;
      });
  },
});

export const { userUpdated } = usersSlice.actions;

export default usersSlice.reducer;

export const {
  selectAll: selectAllUsers,
} = usersAdapter.getSelectors<RootState>((state) => state.users);

export const selectUsersByName = createSelector(
  [selectAllUsers, (_state: RootState, userName: string) => userName],
  (users, userName) =>
    users.filter((user) => user.name.toLowerCase().includes(userName)),
);

export const selectUserById = createSelector(
  [
    selectAllUsers,
    (state: RootState, id: number) => ({ state: state.users, id }),
  ],
  (users, { state, id }) => {
    const user = users.find((user) => user.id === id);
    const formData = {
      name: user?.name || '',
      username: user?.username || '',
      email: user?.email || '',
      phone: user?.phone || '',
      street: user?.address.street || '',
      suite: user?.address.suite || '',
      city: user?.address.city || '',
      zipcode: user?.address.zipcode || '',
    };

    const userId = state.ids.find((el) => state.entities[el]?.id === id);
    return { userId, formData };
  },
);
