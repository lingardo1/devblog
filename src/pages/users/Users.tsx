import { useSelector } from 'react-redux';
import React, { FC, memo, useState } from 'react';
import { SearchOutlined } from '@material-ui/icons';
import { Grid, Input, InputAdornment } from '@material-ui/core';

import { selectUsersByName } from './usersSlice';
import UserListItem from '../users/UserListItem';
import { RootState } from '../../store/rootReducer';
import './styles.scss';

const Users: FC = () => {
  const [searchedName, setSearchedName] = useState('');

  const filteredUsers = useSelector((state: RootState) =>
    selectUsersByName(state, searchedName),
  );

  return (
    <>
      <div>
        <Input
          id="input-with-icon-adornment"
          startAdornment={
            <InputAdornment position="start">
              <SearchOutlined />
            </InputAdornment>
          }
          onChange={(e) => setSearchedName(e.target.value.trim().toLowerCase())}
        />
      </div>
      <div className="user-container">
        <Grid container spacing={2}>
          {filteredUsers.map((user) => (
            <UserListItem key={user.id} user={user} />
          ))}
        </Grid>
      </div>
    </>
  );
};

export default memo(Users);
