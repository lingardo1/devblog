import { combineReducers } from '@reduxjs/toolkit';

import usersReducer from '../pages/users/usersSlice';
import postsReducer from '../pages/posts/postsSlice';
import commentsReducer from '../components/Comments/commentsSlice';

const rootReducer = combineReducers({
  users: usersReducer,
  posts: postsReducer,
  comments: commentsReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
