import { useDispatch, useSelector } from 'react-redux';
import React, { FC, useEffect, useMemo, useState } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { createMuiTheme, CssBaseline, ThemeProvider } from '@material-ui/core';

import Posts from './pages/posts/Posts';
import Users from './pages/users/Users';
import { themeString } from './themes/themes';
import { RootState } from './store/rootReducer';
import Header from './components/Header/Header';
import UserInfo from './pages/userInfo/UserInfo';
import { fetchUsers } from './pages/users/usersSlice';
import { StorageTypes } from './utils/types/storage.types';

const App: FC = () => {
  const dispatch = useDispatch();
  const { status } = useSelector((state: RootState) => state.users);

  useEffect(() => {
    if (status === 'idle') {
      dispatch(fetchUsers());
    }
  }, [status, dispatch]);

  const savedTheme = localStorage.getItem(StorageTypes.Theme) === 'dark';

  const [darkMode, setDarkMode] = useState<boolean>(savedTheme);

  const theme = useMemo(
    () => createMuiTheme({ palette: { type: themeString(darkMode) } }),
    [darkMode],
  );

  const toggleDarkMode = (checked: boolean) => {
    if (checked === null) {
      setDarkMode(savedTheme);
    } else {
      setDarkMode(checked);
      localStorage.setItem(StorageTypes.Theme, themeString(checked));
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <BrowserRouter>
        <Header darkMode={darkMode} toggleDarkMode={toggleDarkMode} />
        <div className="page-wrapper">
          <Switch>
            <Route path="/posts">
              <Posts />
            </Route>
            <Route path="/user-info/:id">
              <UserInfo />
            </Route>
            <Route exact path="/users">
              <Users />
            </Route>
            <Route exact path="/">
              <Redirect to="/users" />
            </Route>
          </Switch>
        </div>
      </BrowserRouter>
    </ThemeProvider>
  );
};

export default App;
