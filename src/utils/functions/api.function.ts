import { ApiMethod } from '../../config/api.config';

export const send = <T>(url: string, method: ApiMethod): Promise<T> => {
  return fetch(url, { method }).then((response) => {
    if (!response.ok) {
      throw new Error(response.statusText);
    }
    return response.json() as Promise<T>;
  });
};
