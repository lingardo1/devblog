export type FetchData<T> = {
  status: 'loading' | 'idle' | 'done';
  data: T;
  error: any;
};
