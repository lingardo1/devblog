export const Patterns = {
  FullName: /^[a-zA-Z'-\s]+(?![a-zA-Z]+)$|^[a-zA-Z'-\s]+(?![a-zA-Z]+)$/,
  Title: /^[a-zA-Z'.-\s]+(?![a-zA-Z]+)$|^[a-zA-Z'.-\s]+(?![a-zA-Z]+)$/,
  UserName: /^(?=.{4,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?![_.])$/,
};
