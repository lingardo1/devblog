import { SortTypes } from '../types/sort.types';

export const getSortableData = <T>(
  items: T[],
  key: keyof T,
  direction: SortTypes,
): T[] => {
  const sortableItems = [...items];

  sortableItems.sort(
    (a, b) =>
      (a[key] < b[key] ? -1 : 1) * (direction === SortTypes.Asc ? 1 : -1),
  );

  return sortableItems;
};
