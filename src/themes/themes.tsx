import { createMuiTheme } from '@material-ui/core';

const darkTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main: '#303030',
    },
    background: {
      default: '#363636',
    },
  },
});

const lightTheme = createMuiTheme({
  palette: {
    type: 'light',
    primary: {
      main: '#2b3a42',
    },
    background: {
      default: '#f5f5f5',
    },
  },
});

const themes = {
  dark: darkTheme,
  light: lightTheme,
};

export const themeString = (b: boolean): 'dark' | 'light' =>
  b ? 'dark' : 'light';

export default themes;
