import {
  createSlice,
  createSelector,
  createAsyncThunk,
  createEntityAdapter,
} from '@reduxjs/toolkit';

import { urls } from '../../config/api.config';
import { RootState } from '../../store/rootReducer';
import { PostComment } from '../../pages/posts/types';
import { send } from '../../utils/functions/api.function';

const commentsAdapter = createEntityAdapter<PostComment>();

const initialState = commentsAdapter.getInitialState({
  status: 'idle',
  error: null,
});

export const fetchComments = createAsyncThunk(
  'comments/fetchComments',
  (postId: number): Promise<PostComment[]> =>
    send(urls.comments(postId), 'GET'),
);

const commentsSlice = createSlice({
  name: 'comments',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchComments.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchComments.fulfilled, (state, action) => {
        state.status = 'succeeded';
        commentsAdapter.upsertMany(state, action.payload);
      })
      .addCase(fetchComments.rejected, (state) => {
        state.status = 'failed';
        // state.error = action.payload;
      });
  },
});

export default commentsSlice.reducer;

export const {
  selectAll: selectAllComments,
  selectById: selectCommentsById,
  selectIds: selectCommentsIds,
} = commentsAdapter.getSelectors<RootState>((state) => state.comments);

export const selectCommentsByPostId = createSelector(
  [selectAllComments, (_state: RootState, postId: number) => postId],
  (comments, postId) => {
    return comments.filter((comment) => comment.postId === postId);
  },
);
