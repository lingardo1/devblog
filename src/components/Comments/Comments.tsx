import React, { FC, memo, useEffect } from 'react';
import {
  List,
  Theme,
  Divider,
  ListItem,
  Typography,
  makeStyles,
  ListItemText,
  createStyles,
  CircularProgress,
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';

import { RootState } from '../../store/rootReducer';
import { fetchComments, selectCommentsByPostId } from './commentsSlice';
import './styles.scss';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      backgroundColor: theme.palette.background.paper,
    },
    inline: {
      display: 'inline',
    },
    commentTitle: {
      textTransform: 'capitalize',
    },
  }),
);

type Props = {
  postId: number;
};

const Comments: FC<Props> = ({ postId }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const allComments = useSelector((state: RootState) =>
    selectCommentsByPostId(state, postId),
  );
  const { status } = useSelector((state: RootState) => state.comments);

  useEffect(() => {
    if (!allComments.length) {
      dispatch(fetchComments(postId));
    }
  }, [dispatch, postId, allComments]);

  return (
    <div className="comment-container">
      {status !== 'loading' ? (
        <List className={classes.root}>
          {allComments.map((item) => (
            <div key={item.id}>
              <ListItem alignItems="flex-start">
                <ListItemText
                  className={classes.commentTitle}
                  primary={item.name}
                  secondary={
                    <Typography
                      component="span"
                      variant="body2"
                      className={classes.inline}
                      color="textPrimary">
                      {item.body}
                    </Typography>
                  }
                />

                <Typography
                  component="span"
                  variant="body2"
                  className={classes.inline}
                  color="textPrimary">
                  {item.email}
                </Typography>
              </ListItem>
              <Divider component="li" />
            </div>
          ))}
        </List>
      ) : (
        <CircularProgress />
      )}
    </div>
  );
};

export default memo(Comments);
