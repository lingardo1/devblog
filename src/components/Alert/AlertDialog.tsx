import {
  Card,
  Modal,
  Theme,
  makeStyles,
  CardHeader,
  Typography,
  CardActions,
  CardContent,
  createStyles,
} from '@material-ui/core';
import React, { FC, memo } from 'react';
import Button from '@material-ui/core/Button';

type Props = {
  open: boolean;
  handleClose: () => void;
  handleConfirm: () => void;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paper: {
      outline: 'none',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      backgroundColor: theme.palette.background.paper,
    },
    actions: {
      justifyContent: 'flex-end',
    },
  }),
);

const AlertDialog: FC<Props> = ({ open, handleClose, handleConfirm }) => {
  const classes = useStyles();
  return (
    <>
      <Modal
        open={open}
        className={classes.modal}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <Card className={classes.paper}>
          <CardHeader title="Are you sure?" />
          <CardContent>
            <Typography color="textSecondary">
              You will not be able to recover this post!
            </Typography>
          </CardContent>
          <CardActions className={classes.actions}>
            <Button onClick={handleClose} color="default" autoFocus>
              No
            </Button>
            <Button onClick={handleConfirm} color="default">
              Yes
            </Button>
          </CardActions>
        </Card>
      </Modal>
    </>
  );
};

export default memo(AlertDialog);
