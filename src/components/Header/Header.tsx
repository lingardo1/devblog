import {
  Switch,
  Button,
  AppBar,
  Toolbar,
  Container,
  makeStyles,
  Typography,
  FormControlLabel,
} from '@material-ui/core';
import React, { FC, memo } from 'react';
import { NavLink, useLocation } from 'react-router-dom';
import DesktopMacIcon from '@material-ui/icons/DesktopMac';

import './styles.scss';

type Props = {
  darkMode: boolean;
  toggleDarkMode: (isDark: boolean) => void;
};

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
  title: {
    flexGrow: 1,
  },
  icon: {
    marginRight: 10,
  },
  logo: {
    margin: 0,
    width: 'auto',
    display: 'flex',
    color: 'inherit',
    alignItems: 'center',
    textDecoration: 'none',
  },
  themeSwitchLabel: {
    marginLeft: 10,
  },
});

const Header: FC<Props> = ({ darkMode, toggleDarkMode }) => {
  const classes = useStyles();
  const location = useLocation();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar style={{ display: 'flex', justifyContent: 'space-between' }}>
          <Container component={NavLink} to="/" className={classes.logo}>
            <DesktopMacIcon className={classes.icon} />
            <Typography variant="h6" className={classes.title}>
              Dev blog
            </Typography>
          </Container>
          <div>
            {location.pathname === '/posts' ? (
              <Button color="inherit" component={NavLink} to="/users">
                Users
              </Button>
            ) : (
              <Button color="inherit" component={NavLink} to="/posts">
                Posts
              </Button>
            )}
            <FormControlLabel
              label="Dark mode"
              className={classes.themeSwitchLabel}
              control={
                <Switch
                  color="default"
                  checked={darkMode}
                  onChange={(_e, checked) => toggleDarkMode(checked)}
                />
              }
            />
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default memo(Header);
